import home from './components/home.vue'
import edit from './components/edit.vue'

const routes = [
    {
        path: '/', component: home
    },
    {
        path: '/edit/', component: edit
    },
    {
        path: '/edit/:id', component: edit
    },
    {
        path: '*', redirect: '/'
    }
]

export default routes;