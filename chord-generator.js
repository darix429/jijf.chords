const fs = require('fs')

const collection = {
    base: ["A", "B", "C", "D", "E", "F", "G"],
    ext: ["M", "m", "maj", "min", "dim", "aug", "sus", "2", "3", "4", "5", "6", "7", "9", "10", "11", "12", "13", "14", "15"],
    maj: [],
    min: [],
    dim: [],
    aug: []
}

// add sharp and flats
collection.base.forEach((c)=>{
    if (c != "E" && c != "B") {
        collection.base.push(c+"#")
    }
    if (c != "C" && c != "F") {
        collection.base.push(c+"b")
    }
})
collection.base.forEach(c=>{
    collection.maj.push(c + "maj")
    collection.min.push(c + "min")
    collection.dim.push(c + "dim")
    collection.aug.push(c + "aug")
})

console.log(collection)
fs.writeFile("chords.json", JSON.stringify(collection), function(er) {
    console.log("DONE!");
    console.error("ERROR",er);
})